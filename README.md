# Cosmacs

This is my personal Emacs configuration which I use daily for a variety of different tasks. This is mostly for my personal use and contains things which I like.

The main configuration is located in the `config.org` file. The `org-babel` tangling is placed in `init.el` and some very early initialization is configured in `early-init.el`.
